﻿using System;

namespace HernianUtils
{
    public static class StringUtils
    {
        public static string Formats(this string format, params object[] args)
        {
            return string.Format(format, args);
        }

        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }
    }
}
