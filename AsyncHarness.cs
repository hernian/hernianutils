﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HernianUtils
{
    public class AsyncHarness
    {
        private TaskCompletionSource<object> _tcs = new TaskCompletionSource<object>();
        private CancellationTokenSource _ctsCommon = new CancellationTokenSource();
        private HashSet<CancellationTokenSource> _ctsSet = new HashSet<CancellationTokenSource>();
        private int _countTask = 0;

        public AsyncHarness()
        {

        }

        public CancellationToken CancellationToken
        {
            get
            {
                return _ctsCommon.Token;
            }
        }

        public bool NoTask
        {
            get
            {
                return _countTask == 0;
            }
        }

        public void Cancel()
        {
            foreach (var cts in _ctsSet)
            {
                if (cts.IsCancellationRequested == false)
                {
                    cts.Cancel();
                }
            }
            if (_ctsCommon.IsCancellationRequested == false)
            {
                _ctsCommon.Cancel();
            }
        }

        public Task WhenAll()
        {
            return _tcs.Task;
        }

        public async void DoAsync(Func<Task> funcAsync, Action<Exception> onException = null)
        {
            _ctsCommon.Token.ThrowIfCancellationRequested();
            try
            {
                ++_countTask;
                await funcAsync(); 
            }
            catch (Exception ex)
            {
                if (onException == null)
                {
                    throw ex;
                }
                onException(ex);
            }
            finally
            {
                --_countTask;
                if ((_countTask == 0) && _ctsCommon.IsCancellationRequested)
                {
                    _tcs.SetResult(null);
                }
            }
        }

        public async Task<T> DoAsync<T>(Func<Task<T>> funcAsync)
        {
            _ctsCommon.Token.ThrowIfCancellationRequested();
            try
            {
                ++_countTask;
                var r = await funcAsync();
                return r;
            }
            finally
            {
                --_countTask;
                if ((_countTask == 0) && _ctsCommon.IsCancellationRequested)
                {
                    _tcs.SetResult(null);
                }
            }
        }
    }
}
